# -*- coding: utf-8 -*-
from import  import QtWidgets
from gui import Ui_MainWindow
import sys
import data
import smtplib
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from email.mime.text import MIMEText
from PyQt5.QtCore import QDate, QDateTime, QTime
from PyQt5.QtWidgets import *


inventory = {}
place_time = {}


def smail(a, aQ, amail):
    alist = []
    if len(a) == len(aQ):
        for i in range(len(a)):
            alist.append(str(a[i]) + '口味' + str(aQ[i]) + '個')
    else:
        alist.extend(a)
    for bagel in alist:
        amail += ',' + bagel
    amail = amail[1:]
    return amail


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.dateTimeEdit.setDate(QDate.currentDate())
        choices = ['地瓜', '香蕉巧克力', '咖啡', '全麥起司']
        self.ui.comboBox.addItems(choices)
        self.ui.comboBox.currentIndexChanged.connect(self.display)
        self.display()
        choices2 = ['原味', '地瓜', '紅豆', '堅果', '巧克力', 'OREO', '蔓越莓', '蒜味', '辣椒']
        self.ui.comboBox_2.addItems(choices2)
        self.ui.comboBox_2.currentIndexChanged.connect(self.display)
        self.display()
        self.ui.pushButton.clicked.connect(self.send_mail)
        self.ui.pushButton_2.clicked.connect(self.add)
        self.ui.pushButton_3.clicked.connect(self.place)
        self.ui.pushButton_4.clicked.connect(self.clean)
        self.ui.pushButton_5.clicked.connect(self.rank)
        self.ui.pushButton_6.clicked.connect(self.refresh)
        self.ui.pushButton_7.clicked.connect(self.clean_place)
        self.ui.pushButton_8.clicked.connect(self.add_place)
        zero = '0'
        for i in range(0, 15):
            self.ui.tableWidget.setItem(i, 1, QtWidgets.QTableWidgetItem(zero))
        self.ui.tableWidget.setColumnCount(2)
        self.ui.tableWidget.setRowCount(15)
        self.ui.tableWidget.verticalHeader().setVisible(False)
        self.ui.tableWidget_3.setEnabled(False)


    def display(self):
        self.ui.label_7.setText('{} [{}]'.format(self.ui.comboBox.currentText(), self.ui.comboBox_2.currentText()))

    def refresh(self):
        for i in range(0, 14):
            self.ui.tableWidget.setItem(i, 1, QtWidgets.QTableWidgetItem('0'))
        self.ui.tableWidget.setItem(0, 0, QtWidgets.QTableWidgetItem('地瓜 [原味]'))
        self.ui.tableWidget.setItem(1, 0, QtWidgets.QTableWidgetItem('地瓜 [地瓜]'))
        self.ui.tableWidget.setItem(2, 0, QtWidgets.QTableWidgetItem('地瓜 [紅豆]'))
        self.ui.tableWidget.setItem(3, 0, QtWidgets.QTableWidgetItem('地瓜 [堅果]'))
        self.ui.tableWidget.setItem(4, 0, QtWidgets.QTableWidgetItem('香蕉巧克力 [原味]'))
        self.ui.tableWidget.setItem(5, 0, QtWidgets.QTableWidgetItem('香蕉巧克力 [巧克力]'))
        self.ui.tableWidget.setItem(6, 0, QtWidgets.QTableWidgetItem('香蕉巧克力 [OREO]'))
        self.ui.tableWidget.setItem(7, 0, QtWidgets.QTableWidgetItem('香蕉巧克力 [蔓越莓]'))
        self.ui.tableWidget.setItem(8, 0, QtWidgets.QTableWidgetItem('咖啡 [原味]'))
        self.ui.tableWidget.setItem(9, 0, QtWidgets.QTableWidgetItem('咖啡 [巧克力]'))
        self.ui.tableWidget.setItem(10, 0, QtWidgets.QTableWidgetItem('咖啡 [OREO]'))
        self.ui.tableWidget.setItem(11, 0, QtWidgets.QTableWidgetItem('咖啡 [堅果]'))
        self.ui.tableWidget.setItem(12, 0, QtWidgets.QTableWidgetItem('全麥起司 [原味]'))
        self.ui.tableWidget.setItem(13, 0, QtWidgets.QTableWidgetItem('全麥起司 [蒜味]'))
        self.ui.tableWidget.setItem(14, 0, QtWidgets.QTableWidgetItem('全麥起司 [胡椒]'))

        for bagel in inventory:
            bagel_Q = str(inventory[bagel])
            if bagel == '地瓜 [原味]':
                self.ui.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '地瓜 [地瓜]':
                self.ui.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '地瓜 [紅豆]':
                self.ui.tableWidget.setItem(2, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '地瓜 [堅果]':
                self.ui.tableWidget.setItem(3, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '香蕉巧克力 [原味]':
                self.ui.tableWidget.setItem(4, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '香蕉巧克力 [巧克力]':
                self.ui.tableWidget.setItem(5, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '香蕉巧克力 [OREO]':
                self.ui.tableWidget.setItem(6, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '香蕉巧克力 [蔓越莓]':
                self.ui.tableWidget.setItem(7, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '咖啡 [原味]':
                self.ui.tableWidget.setItem(8, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '咖啡 [巧克力]':
                self.ui.tableWidget.setItem(9, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '咖啡 [OREO]':
                self.ui.tableWidget.setItem(10, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '咖啡 [堅果]':
                self.ui.tableWidget.setItem(11, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '全麥起司 [原味]':
                self.ui.tableWidget.setItem(12, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '全麥起司 [蒜味]':
                self.ui.tableWidget.setItem(13, 1, QtWidgets.QTableWidgetItem(bagel_Q))
            elif bagel == '全麥起司 [胡椒]':
                self.ui.tableWidget.setItem(14, 1, QtWidgets.QTableWidgetItem(bagel_Q))

    def clean_place(self):
        self.ui.tableWidget_3.clearContents()
        place_time.clear()
        start = 0
        for t in place_time:
            place = str(place_time[t])
            self.ui.tableWidget_3.setItem(start, 0, QtWidgets.QTableWidgetItem(place))
            self.ui.tableWidget_3.setItem(start, 1, QtWidgets.QTableWidgetItem(t))
            start += 1

    def add_place(self):
        Location = str(self.ui.lineEdit.text())
        time = str(self.ui.dateTimeEdit.text())
        place_time[time] = Location
        start = 0
        for t in place_time:
            place = str(place_time[t])
            self.ui.tableWidget_3.setItem(start, 0, QtWidgets.QTableWidgetItem(place))
            self.ui.tableWidget_3.setItem(start, 1, QtWidgets.QTableWidgetItem(t))
            start += 1

    def rank(self):
        buynum = {}
        for name in data.dict_name_flavor:
            for flavor in data.dict_name_flavor[name]:
                if flavor in buynum:
                    if data.dict_name_flavor[name][flavor] > 0:
                        buynum[flavor] += int(data.dict_name_flavor[name][flavor])
                else:
                    if data.dict_name_flavor[name][flavor] > 0:
                        buynum[flavor] = int(data.dict_name_flavor[name][flavor])
        rank = dict(sorted(buynum.items(), key=lambda x: x[1], reverse=True))
        start = 0
        for flavor in rank:
            flavor_Q = str(rank[flavor])
            self.ui.tableWidget_2.setItem(start, 0, QtWidgets.QTableWidgetItem(flavor))
            self.ui.tableWidget_2.setItem(start, 1, QtWidgets.QTableWidgetItem(flavor_Q))
            start += 1

    def add(self):
        text = str(self.ui.comboBox.currentText() + ' [' + self.ui.comboBox_2.currentText() + ']')
        number = self.ui.spinBox.text()
        inventory[text] = int(number)
        if text == '地瓜 [原味]':
            self.ui.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '地瓜 [地瓜]':
            self.ui.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '地瓜 [紅豆]':
            self.ui.tableWidget.setItem(2, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '地瓜 [堅果]':
            self.ui.tableWidget.setItem(3, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '香蕉巧克力 [原味]':
            self.ui.tableWidget.setItem(4, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '香蕉巧克力 [巧克力]':
            self.ui.tableWidget.setItem(5, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '香蕉巧克力 [OREO]':
            self.ui.tableWidget.setItem(6, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '香蕉巧克力 [蔓越莓]':
            self.ui.tableWidget.setItem(7, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '咖啡 [原味]':
            self.ui.tableWidget.setItem(8, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '咖啡 [巧克力]':
            self.ui.tableWidget.setItem(9, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '咖啡 [OREO]':
            self.ui.tableWidget.setItem(10, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '咖啡 [堅果]':
            self.ui.tableWidget.setItem(11, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '全麥起司 [原味]':
            self.ui.tableWidget.setItem(12, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '全麥起司 [蒜味]':
            self.ui.tableWidget.setItem(13, 1, QtWidgets.QTableWidgetItem(number))
        elif text == '全麥起司 [胡椒]':
            self.ui.tableWidget.setItem(14, 1, QtWidgets.QTableWidgetItem(number))
        else:
            del inventory[text]

    def clean(self):
        zero = '0'
        for i in range(0, 15):
            self.ui.tableWidget.setItem(i, 1, QtWidgets.QTableWidgetItem(zero))
        for bagel in inventory:
            inventory[bagel] = 0

    def place(self):
        place_list = []
        time_list = []
        place_time_list = []
        location = ''
        for time in place_time:
            time_list.append(time)
            place_list.append(place_time[time])
        if len(time_list) == len(place_list):
            for i in range(len(time_list)):
                place_time_list.append(str(time_list[i]) + '於' + str(place_list[i]))
        for x in place_time_list:
            location += '\n' + x
        location = location[1:]
        mime = MIMEText(("感謝您的訂購!\n我將會在下列時間販售Bagel:\n{}\n希望能在該時間點跟您順利交貨!!!".format(location)), "plain",
                        "utf-8")
        mime["Subject"] = "My Bagel 販售地點告知信"  # 撰寫郵件標題

        mime["From"] = "My Bagel 官方信箱"
        smtp = smtplib.SMTP("smtp.gmail.com", 587)
        smtp.ehlo()
        smtp.starttls()
        smtp.login('mybagel1', 'mybagel12321')
        from_addr = 'mybagel1@gmail.com'
        msg = mime.as_string()
        for name in data.dict_name_email:
            to_addr = name
            status = smtp.sendmail(from_addr, to_addr, msg)
            if status == {}:
                print("{} 郵件傳送成功!".format(name))
            else:
                print("{}郵件傳送失敗!".format(name))
        smtp.quit()
        print('郵件傳送完畢')

    def send_mail(self):
        name_list = []
        buy_list = []
        for name in data.dict_name_flavor:
            buy = []
            soldout = []
            buyQ = []
            soldoutQ = []
            failure = 0
            ordernumber = 0

            for bagel in data.dict_name_flavor[name]:
                if data.dict_name_flavor[name][bagel] > 0:
                    ordernumber += 2
            for flavor in data.dict_name_flavor[name]:
                if flavor in inventory:
                    if data.dict_name_flavor[name][flavor] <= inventory[flavor]:
                        failure += 0
                    else:
                        if inventory[flavor] > 0:
                            failure += 1
                        else:
                            failure += 2
                else:
                    inventory[flavor] = 0
                    if data.dict_name_flavor[name][flavor] > 0:
                        failure += 2

            if failure == ordernumber:
                for flavor in data.dict_name_flavor[name]:
                    if data.dict_name_flavor[name][flavor] > 0:
                        soldout.append(flavor)
                        soldoutQ.append(data.dict_name_flavor[name][flavor])
                texttype = 2
            elif ordernumber > failure > 0:
                for flavor in data.dict_name_flavor[name]:
                    if data.dict_name_flavor[name][flavor] <= inventory[flavor]:
                        if data.dict_name_flavor[name][flavor] > 0:
                            inventory[flavor] -= data.dict_name_flavor[name][flavor]
                            buy.append(flavor)
                            buyQ.append(data.dict_name_flavor[name][flavor])
                            number2 = str(inventory[flavor])
                            if flavor == '地瓜 [原味]':
                                self.ui.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '地瓜 [地瓜]':
                                self.ui.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '地瓜 [紅豆]':
                                self.ui.tableWidget.setItem(2, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '地瓜 [堅果]':
                                self.ui.tableWidget.setItem(3, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '香蕉巧克力 [原味]':
                                self.ui.tableWidget.setItem(4, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '香蕉巧克力 [巧克力]':
                                self.ui.tableWidget.setItem(5, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '香蕉巧克力 [OREO]':
                                self.ui.tableWidget.setItem(6, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '香蕉巧克力 [蔓越莓]':
                                self.ui.tableWidget.setItem(7, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '咖啡 [原味]':
                                self.ui.tableWidget.setItem(8, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '咖啡 [巧克力]':
                                self.ui.tableWidget.setItem(9, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '咖啡 [OREO]':
                                self.ui.tableWidget.setItem(10, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '咖啡 [堅果]':
                                self.ui.tableWidget.setItem(11, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '全麥起司 [原味]':
                                self.ui.tableWidget.setItem(12, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '全麥起司 [蒜味]':
                                self.ui.tableWidget.setItem(13, 1, QtWidgets.QTableWidgetItem(number2))
                            elif flavor == '全麥起司 [胡椒]':
                                self.ui.tableWidget.setItem(14, 1, QtWidgets.QTableWidgetItem(number2))
                            else:
                                del inventory[flavor]
                    else:
                        if inventory[flavor] == 0:
                            if data.dict_name_flavor[name][flavor] > 0:
                                soldout.append(flavor)
                                soldoutQ.append(data.dict_name_flavor[name][flavor])
                        elif inventory[flavor] > 0:
                            if data.dict_name_flavor[name][flavor] > 0:
                                buy.append(flavor)
                                buyQ.append(inventory[flavor])
                                soldout.append(flavor)
                                soldoutQ.append(data.dict_name_flavor[name][flavor] - inventory[flavor])
                                number2 = str(inventory[flavor])
                                inventory[flavor] = 0
                                if flavor == '地瓜 [原味]':
                                    self.ui.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '地瓜 [地瓜]':
                                    self.ui.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '地瓜 [紅豆]':
                                    self.ui.tableWidget.setItem(2, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '地瓜 [堅果]':
                                    self.ui.tableWidget.setItem(3, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '香蕉巧克力 [原味]':
                                    self.ui.tableWidget.setItem(4, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '香蕉巧克力 [巧克力]':
                                    self.ui.tableWidget.setItem(5, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '香蕉巧克力 [OREO]':
                                    self.ui.tableWidget.setItem(6, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '香蕉巧克力 [蔓越莓]':
                                    self.ui.tableWidget.setItem(7, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '咖啡 [原味]':
                                    self.ui.tableWidget.setItem(8, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '咖啡 [巧克力]':
                                    self.ui.tableWidget.setItem(9, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '咖啡 [OREO]':
                                    self.ui.tableWidget.setItem(10, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '咖啡 [堅果]':
                                    self.ui.tableWidget.setItem(11, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '全麥起司 [原味]':
                                    self.ui.tableWidget.setItem(12, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '全麥起司 [蒜味]':
                                    self.ui.tableWidget.setItem(13, 1, QtWidgets.QTableWidgetItem(number2))
                                elif flavor == '全麥起司 [胡椒]':
                                    self.ui.tableWidget.setItem(14, 1, QtWidgets.QTableWidgetItem(number2))
                                else:
                                    del inventory[flavor]
                texttype = 1
            else:
                texttype = 0
                for flavor in data.dict_name_flavor[name]:
                    if data.dict_name_flavor[name][flavor] > 0:
                        buy.append(flavor)
                        buyQ.append(data.dict_name_flavor[name][flavor])
                        inventory[flavor] -= data.dict_name_flavor[name][flavor]
                        number2 = str(inventory[flavor])
                        if flavor == '地瓜 [原味]':
                            self.ui.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '地瓜 [地瓜]':
                            self.ui.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '地瓜 [紅豆]':
                            self.ui.tableWidget.setItem(2, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '地瓜 [堅果]':
                            self.ui.tableWidget.setItem(3, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '香蕉巧克力 [原味]':
                            self.ui.tableWidget.setItem(4, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '香蕉巧克力 [巧克力]':
                            self.ui.tableWidget.setItem(5, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '香蕉巧克力 [OREO]':
                            self.ui.tableWidget.setItem(6, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '香蕉巧克力 [蔓越莓]':
                            self.ui.tableWidget.setItem(7, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '咖啡 [原味]':
                            self.ui.tableWidget.setItem(8, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '咖啡 [巧克力]':
                            self.ui.tableWidget.setItem(9, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '咖啡 [OREO]':
                            self.ui.tableWidget.setItem(10, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '咖啡 [堅果]':
                            self.ui.tableWidget.setItem(11, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '全麥起司 [原味]':
                            self.ui.tableWidget.setItem(12, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '全麥起司 [蒜味]':
                            self.ui.tableWidget.setItem(13, 1, QtWidgets.QTableWidgetItem(number2))
                        elif flavor == '全麥起司 [胡椒]':
                            self.ui.tableWidget.setItem(14, 1, QtWidgets.QTableWidgetItem(number2))
                        else:
                            del inventory[flavor]

            buymail = ''
            soldoutmail = ''

            if texttype == 0:
                mime = MIMEText("感謝您的訂購! 您已訂購成功。\n您所訂的項目是{}。\n如有問題請回覆給我們知道!\n最後也請密切關注信箱，在領取前我們會再以信箱告知!!!".format(smail(buy, buyQ, buymail)), "plain", "utf-8")
                mime["Subject"] = "My Bagel 訂購成功郵件"  # 撰寫郵件標題
                name_list.append(name)
                buy_list.append((smail(buy, buyQ, buymail)))
            elif texttype == 1:
                mime = MIMEText("感謝您的訂購! 您已成功訂購{}口味。\n但很抱歉您要的{}口味已被購完。\n我們非常抱歉，請您再次填寫表單確定您要訂購的項目!".format(smail(buy, buyQ, buymail), smail(soldout, soldoutQ, soldoutmail)), "plain", "utf-8")
                mime["Subject"] = "My Bagel  口味售完道歉信"  # 撰寫郵件標題
                name_list.append(name)
                buy_list.append((smail(buy, buyQ, buymail)))
            else:
                mime = MIMEText("感謝您的訂購! 很抱歉您要的{}口味已被購完。\n我們非常抱歉，請您再次填寫表單確定您要訂購的項目!".format(smail(soldout, soldoutQ, soldoutmail)), "plain", "utf-8")
                mime["Subject"] = "My Bagel  訂購失敗郵件"

            mime["From"] = "My Bagel 官方信箱"
            smtp = smtplib.SMTP("smtp.gmail.com", 587)
            smtp.ehlo()
            smtp.starttls()
            smtp.login('mybagel1', 'mybagel12321')
            from_addr = 'mybagel1@gmail.com'
            msg = mime.as_string()
            to_addr = name
            status = smtp.sendmail(from_addr, to_addr, msg)
            if status == {}:
                print("{} 郵件傳送成功!".format(name))
            else:
                print("{} 郵件傳送失敗!".format(name))
            smtp.quit()
        print('郵件傳送完畢')

        auth_json_path = 'credit.json'
        gss_scopes = ['https://spreadsheets.google.com/feeds']

        # 連線
        credentials = ServiceAccountCredentials.from_json_keyfile_name(auth_json_path, gss_scopes)
        gss_client = gspread.authorize(credentials)

        # 開啟 Google Sheet 資料表
        spreadsheet_key = '1xkO5HYJww1J7x9uLDKZhQnWkKb1vDIghxq0J8L60ez0'

        # 建立工作表1
        sheet = gss_client.open_by_key(spreadsheet_key).sheet1
        sheet.clear()  # 清除 Google Sheet 資料表內容
        sheet.update_cell(1, 1, '顧客名字')
        sheet.update_cell(1, 2, '購買內容')

        # 訂購資訊
        for i in range(len(name_list)):
            sheet.update_cell(i + 2, 1, name_list[i])
            sheet.update_cell(i + 2, 2, buy_list[i])

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())

