#-*-coding:UTF-8 -*-
import json
import copy
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint

scope = ["https://spreadsheets.google.com/feeds"]
creds = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scope)

client = gspread.authorize(creds)

# sheet = client.open("mybagel").sheet1
sheet = client.open_by_key("1hkNw3Mevx94FUJcmNPM-1shC12iBpj2mb-EA7lNY5Ow").sheet1
data = list (sheet.get_all_records())
data0 = copy.deepcopy(data)
dict_main = {}
dict_name_time = {}
dict_name_email = {}
dict_name_flavor = {}
dict_flavor = {}
dict1 = {}
datar = []
for dict0 in data0:
    for flavor in dict0:
        if (dict0[flavor]):
            dict0[flavor] = dict0[flavor]
        else:
            dict0[flavor] = 0
for dict0 in data0:
    i = 0
    for name in datar:
        if (dict0['信箱'] in name.values()):
            for flavor in dict0:
                if ((flavor != '時間戳記') & (flavor != '姓名') & (flavor != '信箱')):
                    name[flavor] += dict0[flavor]
                    i += 1
    if(i == 0):
        datar.append(dict0)
data1 = copy.deepcopy(datar)
data2 = copy.deepcopy(datar)
for dict1 in data1:
    name_str = dict1['信箱']
    dict1.pop('信箱')
    dict_main[name_str] = dict1
for name_str in dict_main:
    t = 0
    dictt = {}
    time_str = dict_main[name_str]['時間戳記']
    if '下' in time_str:
        t = 1
    time_str = time_str.replace(' ','').replace('/','').replace('上','').replace('下','').replace('午','').replace(':','')
    time_int = int(time_str)
    if (time_int < 10000000000000):
        time_int = int(time_int / 100000) * 1000000 + time_int % 100000
        time_str = str(time_int)
    if(t == 1):
        time_int = int(time_str)
        time_int += 120000
        time_str = str(time_int)
    dictt['時間戳記'] = time_str
    dict_name_time[name_str] = dictt
for name_str in dict_main:
    dict_name_email[name_str] = dict_main[name_str]['姓名']
i = 0
for dict2 in data2:
    name_str = dict2['信箱']
    dict2.pop('時間戳記')
    dict2.pop('信箱')
    dict2.pop('姓名')
    for flavor in dict2:
        if(dict2[flavor]):
            dict2[flavor] = dict2[flavor]
        else:
            dict2[flavor] = 0
    dict_name_flavor[name_str] = dict2
    if(i == 0):
        for key in dict2:
            dict_flavor[key] = 0
    for key in dict2:
        dict_flavor[key] += dict2[key]
    i += 1
# print("dict_main : ",dict_main)
# print("dict_name_time : ",dict_name_time)
# print("dict_name_email : ",dict_name_email)
# print("dict_name_flavor : ",dict_name_flavor)
# print("dict_flavor : ",dict_flavor)
# print(data)

